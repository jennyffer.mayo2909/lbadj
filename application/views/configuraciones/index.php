j<script type="text/javascript">
  $("#menu-configuracion").addClass('active');
</script>
<center>

<h3>Gestion de Configuracion</h3>
</center>

<?php if ($listadoConfiguracion): ?>
  <table id="tbl-configuracion" class="table table-stripe table-border table-hover">
<thead>
  <th class="text-center">Id</th>
  <th class="text-center">Nombre de la empresa</th>
  <th class="text-center">Ruc</th>
  <th class="text-center">Telefono</th>
  <th class="text-center">Direccion</th>
  <th class="text-center">Representante</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadoConfiguracion-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_con_aj; ?></td>
      <td class="text-center"><?php echo $temporal->nombre_empresa_aj; ?></td>
      <td class="text-center"><?php echo $temporal->ruc_aj; ?></td>
      <td class="text-center"><?php echo $temporal->telefono_aj; ?></td>
      <td class="text-center"><?php echo $temporal->dirreccion_aj; ?></td>
      <td class="text-center"><?php echo $temporal->representante_aj; ?></td>
      <td>
        <a href="<?php echo site_url("configuraciones/borrar"); ?>/<?php echo $temporal->id_con_aj; ?>" class="btn btn-danger"> <i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <a href="<?php echo site_url("configuraciones/editar"); ?>/<?php echo $temporal->id_con_aj; ?>" class="btn btn-warning"> <i class="glyphicon glyphicon-trash"> Editar</i></a>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<center>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_configuracion">Nuevo Configuracion</button>
</center>
<!-- Modal -->
<div id="modal_configuracion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nuevo Configuracion</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_configuracion" class="" action="<?php echo site_url('configuraciones/guardar'); ?>" method="post">
          <b>Nombre de Empresa:</b>
          <br>
          <input type="text" id="nombre_empresa_aj" name="nombre_empresa_aj" value="" placeholder="ingrese el nombre del configuracion" class="form-control">
          <br>
          <b>Ruc:</b>
          <br>
          <input type="number" id="ruc_aj" name="ruc_aj" value="" placeholder="ingrese el ruc" class="form-control">
          <br>
          <b>telefono:</b>
          <br>
          <input type="number" id="telefono_aj" name="telefono_aj" value="" placeholder="ingrese el telefono" class="form-control">
          <br>
          <b>Direccion:</b>
          <br>
          <input type="text" id="dirreccion_aj" name="dirreccion_aj" value="" placeholder="ingrese la direccion" class="form-control">
          <br>
          <b>Representante:</b>
          <br>
          <input type="text" id="representante_aj" name="representante_aj" value="" placeholder="ingrese el representante" class="form-control">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>

      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_configuracion").validate({
   rules:{
       nombre_empresa_aj:{
         required:true
       },
       ruc_aj:{
         required: true
       },
       telefono_aj:{
         required: true
       },
       dirreccion_aj:{
         required: true
       },
       representante_aj:{
         required: true
       }
   },
   messages:{
     nombre_empresa_aj:{
       required:"ingrese el nombre de la emoresa"
     },
     ruc_aj:{
       required: "ingrese el ruc"
     },
     telefono_aj:{
       required: "ingrese el telefono"
     },
     dirreccion_aj:{
       required: "ingrese una direccion"
     },
     representante_aj:{
       required: "ingrese un represntante"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("configuraciones/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_configuracion").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_producto").modal("hide");
       }
     });
   }
});
</script>
</script>

<script type="text/javascript">
  $('#tbl-configuracion').DataTable();
</script>
