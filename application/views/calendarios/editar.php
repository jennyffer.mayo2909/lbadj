<center>
<form class="" action="<?php echo site_url();?>/calendarios/procesarActualizacion" method="post" >

  <div style="border:3px ; width:40%; margin-top:4px; margin-left:7%; margin-right:7%;">
    <h4> <b>ACTUALIZAR DATOS DEL CALENDARIO</b> </h4> <br>

    <table>
      <tr>
        <td>
            <input type="hidden" name="id_cal_aj" id="id_cal_aj" value="<?php echo $listadoCalendarios->id_cal_aj; ?>">
          <label for="">EQUIPO LOCAL</label>
          <select   name="fk_id_equipo1" id="fk_id_equipo1_aj" >
            <option value="">--Seleccione un equipo--</option>
            <?php if ($listadoEquipos): ?>
              <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
                <option value="<?php echo $equipoTemporal->id_equi_aj; ?>">
                  <?php echo $equipoTemporal->nombre_equi_aj; ?>
                </option>
              <?php endforeach; ?>
            <?php endif; ?>
          </select>
        </td>
        <td>
          <label for="">EQUIPO VISITANTE</label>
          <select   name="fk_id_equipo2_aj" id="fk_id_equipo2_aj" >
            <option value="">--Seleccione un equipo--</option>
            <?php if ($listadoEquipos): ?>
              <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
                <option value="<?php echo $equipoTemporal->id_equi_aj; ?>">
                  <?php echo $equipoTemporal->nombre_equi_aj; ?>
                </option>
              <?php endforeach; ?>
            <?php endif; ?>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <label for="">fecha y hora : </label>
          <input class="form-control" value="<?php echo $listadoCalendarios->fecha_hora_aj; ?>"   onkeypress="return validar(event)"  type="date" name="fecha_hora" id='fecha_hora' placeholder="Ingrese la fecha">
          <br>
        </td>
        <td>
          <label for="">RESULTADOS </label>
          <br>
          <input class="form-control" value="<?php echo $listadoCalendarios->resultado_aj; ?>"  onkeypress="return validar(event)" type="text" name="resultado" id="resultado" placeholder="Ingrese el resultado ">
          <br>
        </td>
      </tr>
      <tr>
        <td>
          <label for="">INFORME </label> <br>
          <input class="form-control" value="<?php echo $listadoCalendarios->informe_aj; ?>"   type="text" name="informe" id="informe" value="" placeholder="Ingrese su informe">
          <br>
        </td>
      </tr>
    </table>
  <button type="submit" name="button" class="btn btn-primary">ACTUALIZAR</button>
    <a href="<?php echo site_url(); ?>/jugadores/index"   class="btn btn-warning"> <i class="fa fa-times"></i>CANCELAR</a>
</div>
</form>
</center>


<!-- ************************************ACTIVANDO EL equipo SELECCIONADO POR EL JUGADOR*********************************** -->
<script type="text/javascript">
  $("#fk_id_equipo1_aj").val("<?php echo $listadoCalendarios->fk_id_equipo1_aj; ?>");
  $("#fk_id_equipo2_aj").val("<?php echo $listadoCalendarios->fk_id_equipo2_aj; ?>");

</script>
