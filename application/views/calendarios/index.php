<br>
<center>
  <h2>REGISTRO DE CalendarioS</h2>

</center>
<br>
<center>
    <a href="<?php echo site_url(); ?>/arbitros/nuevo" class="btn btn-secondary">
    AGREGAR ARBITRO
    </a>
    <br>
    <br>
</center>
      <?php if ($listadoCalendarios): ?>
        <table class="table table-bordered table-striped table-hover" id="tbl-clientes2_aj" >
          <center>
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">EQUIPO LOCAL</th>
            <th class="text-center">EQUIPO VISITANTE</th>
            <th class="text-center">FECHA Y HORA </th>
            <th class="text-center">RESULTADOS </th>
            <th class="text-center">INFORME </th>
            <th class="text-center">ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoCalendarios->result() as $filaTemporal): ?>
            <tr>

              <td class="text-center">
                <?php echo $filaTemporal->id_cal_aj;?>
              </td>

              <td class="text-center">
              <?php echo $filaTemporal->fk_id_equipo1_aj;?>
              </td>

              <td class="text-center">
                <?php echo $filaTemporal->fk_id_equipo2_aj;?>
              </td>

              <td class="text-center">
                <?php
                 echo $filaTemporal->fecha_hora_aj;?>
              </td>
              
              <td class="text-center">
              <?php echo $filaTemporal->resultado_aj;?>
              </td>
              
              <td class="text-center" >
              <?php echo $filaTemporal->informe_aj;?>
              </td>


            <center>
            <td class="text-center">
              <a href="<?php echo site_url(); ?>/calendarios/editar/<?php echo $filaTemporal->id_cal_aj;?>" class="btn btn-warning" ><i class="fa fa-pencil"></i></a>

              <a onclick="return confirm('Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/calendarios/procesarEliminacion/<?php echo $filaTemporal->id_cal_aj;?>" class="btn btn-danger"><i class="fa fa-trash"> </i></a>
            </td>
          </center>
          <?php endforeach; ?>
            </tr>
        </tbody>

      </center>
      </table>

    <?php else: ?>
      <div class="alert alert-damger">
        <h3>NO SE ENCONTRARON JUEGOS REGISTRADOS</h3>
      </div>
    <?php endif; ?>


    <script type="text/javascript">
        function procesarEliminacion(id_cal_aj){
              iziToast.question({
                  timeout: 20000,
                  close: false,
                  overlay: true,
                  displayMode: 'once',
                  id: 'question',
                  zindex: 999,
                  title: 'CONFIRMACIÓN',
                  message: '¿Esta seguro de eliminar el arbitro de forma pernante?',
                  position: 'center',
                  buttons: [
                      ['<button><b>SI</b></button>', function (instance, toast) {

                          instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                          window.location.href=
                          "<?php echo site_url(); ?>/calendarios/procesarEliminacion/"+id_cal_aj;

                      }, true],
                      ['<button>NO</button>', function (instance, toast) {

                          instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                      }],
                  ]
              });
        }
    </script>
  <script type="text/javascript">
    $("#tbl-clientes2_aj").DataTable({
      responsive: true,
   autoWidth: false,
   position: 'center',
      dom: 'Blfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
      language: {
                url: "https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-MX.json"
            }
    });
  </script>
