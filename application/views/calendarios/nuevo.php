<center>
<form class="" action="<?php echo site_url();?>/calendarios/guardarCalendario" method="post" id="frm_nuevo_calendario">
  <div style="border:3px ; width:40%; margin-top:4px; margin-left:7%; margin-right:7%;" type="table">
    <h4> <b>Agregar Calendario</b> </h4> <br>
    <hr>
  <table>
    <tr>
      <td>
        <label for="">EQUIPO LOCAL</label>
        <select name="fk_id_equipo1_aj" id="fk_id_equipo1_aj" required>
          <option value="">--Seleccione un equipo--</option>
          <?php if ($listadoEquipos): ?>
            <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
              <option value="<?php echo $equipoTemporal->id_equi_aj; ?>">
                <?php echo $equipoTemporal->nombre_equi_aj; ?>
              </option>
            <?php endforeach; ?>
          <?php endif; ?>
        </select>
      </td>
      <td>
        <br><br>
        <label for="">EQUIPO VISITANTE</label>
        <select   name="fk_id_equipo2_aj" id="fk_id_equipo2_aj" required>
          <option value="">--Seleccione un equipo--</option>
          <?php if ($listadoEquipos): ?>
            <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
              <option value="<?php echo $equipoTemporal->id_equi_aj; ?>">
                <?php echo $equipoTemporal->nombre_equi_aj; ?>
              </option>
            <?php endforeach; ?>
          <?php endif; ?>
        </select>
      </td>
    </tr>
    <tr>
      <td>
        <label for="">fecha y hora : </label>
        <br>
        <input class="form-control"   type="date" name="fecha_hora_aj" id='fecha_hora_aj' value="" placeholder="Ingrese la fecha">
        <br>
      </td>
      <td>
        <label for="">RESULTADOS  </label>
        <br>
        <input class="form-control"   type="text" name="resultado_aj" id="resultado_aj" value="" placeholder="Ingrese el resultado ">
        <br>
      </td>
    </tr>
    <tr>
      <td>
        <label for="">Informe</label> <br>
        <input class="form-control"    type="text" name="informe_aj" id="informe_aj" value="" placeholder="Ingrese su informe">
        <br>
      </td>
    </tr>
  </table>
    <br>
    <br>

  <button type="submit" name="button" class="btn btn-succes"><i class="fa fa-save"></i></button>
    <a href="<?php echo site_url(); ?>/calendarios/index"class="btn btn-primary"><i class="fa fa-cancel"></i></a>
    <center>
</div>
</form>
</center>


<!-- AGREGAMOS TAG PARA LLAMAR AL FORMULARIO -->
<script type="text/javascript">
    $("#frm_nuevo_calendario").validate({
      rules:{
        fk_id_equipo1_aj:{
          required:true
        },
        fk_id_equipo2:{
          required:true
        },

        hora_fecha_aj:{
          date:true,
          required:true
        },
         resultado_aj:{
           letras:true,
           required:true
         },
        informe_aj:{
          required:true
        }

      },

      messages:{
        fk_id_equipo1_aj:{
          required:"seleccione un equipo"
        },
        fk_id_equipo2_aj:{
          required:"seleccione un equipo"
        },
        hora_fecha_aj:{
          date:"solo valido para formato de fechas",
          required:"Por favor ingrese una fecha"
        },

     resultado_aj:{
     required:"Por favor el resultado"
      },
         informe_aj:{
        letras:"solo valido para letras",
         required:"Por favor ingrese una fecha"
        }

      }
    });
</script>
