
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Bienvenidos
LIGA BARRIAL SAN BUENAVENTURA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- j query -->
      <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/fontawesome.min.css" integrity="sha512-xX2rYBFJSj86W54Fyv1de80DWBq7zYLn2z0I9bIhQG+rxIF6XVJUpdGnsNHWRa6AvP89vtFupEPDP8eZAtu9qA==" crossorigin="anonymous" referrerpolicy="no-referrer" />


  <!-- bootstrap 3 -->
      <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/fonts/icomoon/style.">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.theme.default.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/jquery.fancybox.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap-datepicker.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/fonts/flaticon/font/flaticon.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/aos.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">

  <!-- izytoast -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.js" type="text/javascript"></script>

<!-- datetables -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/dataTables.bootstrap4.min.css" integrity="sha512-PT0RvABaDhDQugEbpNMwgYBCnGCiTZMh9yOzUsJHDgl/dMhD9yjHAwoumnUk3JydV3QTcIkNDuN40CJxik5+WQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.4.0/css/autoFill.dataTables.min.css" integrity="sha512-PT0RvABaDhDQugEbpNMwgYBCnGCiTZMh9yOzUsJHDgl/dMhD9yjHAwoumnUk3JydV3QTcIkNDuN40CJxik5+WQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.4.0/js/dataTables.autoFill.min.js" integrity="sha512-PT0RvABaDhDQugEbpNMwgYBCnGCiTZMh9yOzUsJHDgl/dMhD9yjHAwoumnUk3JydV3QTcIkNDuN40CJxik5+WQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.4.0/js/dataTables.autoFill.min.js" integrity="sha512-PT0RvABaDhDQugEbpNMwgYBCnGCiTZMh9yOzUsJHDgl/dMhD9yjHAwoumnUk3JydV3QTcIkNDuN40CJxik5+WQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js" integrity="sha512-PT0RvABaDhDQugEbpNMwgYBCnGCiTZMh9yOzUsJHDgl/dMhD9yjHAwoumnUk3JydV3QTcIkNDuN40CJxik5+WQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" integrity="sha512-PT0RvABaDhDQugEbpNMwgYBCnGCiTZMh9yOzUsJHDgl/dMhD9yjHAwoumnUk3JydV3QTcIkNDuN40CJxik5+WQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />



<!-- EXPORTAR PDF EXCEL -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-3.6.0/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-html5-2.2.3/b-print-2.2.3/datatables.min.js"></script>
<!-- j validator -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<?php if ($this->session->flashdata('confirmacion')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'CONFIRMACIÓN', //titulo
            '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
            'success' //Tipo de alerta
          )
        });
      </script>
<?php endif; ?>


<?php if ($this->session->flashdata('error')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'ERROR', //titulo
            '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
            'error' //Tipo de alerta
          )
        });
      </script>
<?php endif;?>



</head>

<body>

  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    <header class="site-navbar py-4" role="banner">

      <div class="container">
        <div class="d-flex align-items-center">
          <div class="site-logo">
            <a href=" ">
              <img src="https://ligasanbuenaventura.ec/images/LogoprincipalEquipos/CentroJR.png" alt="Logo">
            </a>
            <li class="dropdown"><a href="<?php echo site_url(); ?>"> <span class="sr-only">(current)</span></a></li>

          </div>

          <div class="ml-auto">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li id="menu-configuracion">
                      <a class="nav-link"
                      href="<?php echo site_url(); ?>/configuraciones/index">
                        <i class="typcn typcn-device-desktop menu-icon"></i>
                        <span class="menu-title">CONFIGURACION</span>
                      </a>
                    </li>
                <li><a href="<?php echo site_url(); ?>/sucursales/index" class="nav-link">SUCURSALES</a></li>
                <li><a href="<?php echo site_url(); ?>/calendarios/index" class="nav-link">CALENDARIOS</a></li>
                <li><a href="<?php echo site_url(); ?>/jugadores/index"   class="nav-link">JUGADORES</a></li>
                <li><a href="<?php echo site_url(); ?>/arbitros/index"   class="nav-link">ARBITROS</a></li>
                <li id="menu-equipos">
                      <a class="nav-link"
                      href="<?php echo site_url(); ?>/equipos/index">
                        <i class="typcn typcn-device-desktop menu-icon"></i>
                        <span class="menu-title">EQUIPOS</span>
                      </a>
                    </li>


                        <li><a href="<?php echo site_url(); ?>/octavos/index"   class="nav-link">OCTAVOS</a></li>
                        <li><a href="<?php echo site_url(); ?>/cuartos/index"   class="nav-link">CUARTOS </a></li>
                          <li><a href="<?php echo site_url(); ?>/semifinales/index"   class="nav-link">SEMIFINAL</a></li>
                        <li><a href="<?php echo site_url(); ?>/finales/index"   class="nav-link">FINAL</a></li>

                <li><a href="<?php echo site_url(); ?>/seguridades/formularioLogin"   class="nav-link">LOGIN</a></li>

              </ul>
            </nav>

            <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black float-right text-black"><span
                class="icon-menu h3 text-black"></span></a>
          </div>
        </div>
      </div>

    </header>
    <div class="hero overlay" style="background-image: url('https://ligasanbuenaventura.ec/images/fondoencuentros/fondo1.jpeg');"  >
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-2 ml-auto">
            <h1 class="text-white"></h1>

          </div>

          <div class="col-lg-5 ml-auto">
            <h1 class="text-white">Bienvenidos
LIGA BARRIAL SAN BUENAVENTURA  </h1>
<p>

‘’Sin fútbol mi vida no valdría nada‘’ Cristiano Ronaldo.  </p>
          </div>
        </div>
      </div>
    </div>
</tbody>
    </div>
