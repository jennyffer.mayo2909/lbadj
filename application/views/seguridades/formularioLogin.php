<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<div style="padding-top:10%">
<form class="" action="<?php echo site_url();?>/seguridades/validarAcceso" method= "post">
  <link rel="icon" type="imagen/jpg" href="https://st2.depositphotos.com/5262531/8721/i/450/depositphotos_87218904-stock-photo-lightning-letter-j.jpg">

  <!-- importacion de jquerry -->
  <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
  <!-- importacion de bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <!--  Importacion de Jquerry Validate -->
  <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js'); ?>">
  </script>
      <!--  Importacion de Jquerry methods-->
  <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js'); ?>">
  </script>
       <!--  Importacion de Jquerry español -->
  <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_PE.min.js'); ?>">
  </script>
  <br>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

  <!-- importacion sweetalert2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <!-- Importacion Bootstrap Select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/i18n/defaults-es_ES.min.js" integrity="sha512-RN/dgJo36dNkKVnb1XGzePP4/8XGa/r+On4XYUy8I1C5z+9SsIEU2rFh6TrunAnddKwtXwMdI0Se8HZxd0GtiQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <?php if ($this->session->flashdata('confirmacion')): ?>
        <script type="text/javascript">
          $(document).ready(function(){
            Swal.fire(
              'CONFIRMACIÓN', //titulo
              '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
              'success' //Tipo de alerta
            )
          });
        </script>
      <?php endif; ?>


      <?php if ($this->session->flashdata('error')): ?>
        <script type="text/javascript">
          $(document).ready(function(){
            Swal.fire(
              'ERROR', //titulo
              '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
              'error' //Tipo de alerta
            )
          });
        </script>
      <?php endif; ?>

<!-- Section: Design Block -->
<section class="">
  <!-- Jumbotron -->
  <div class="px-4 py-5 px-md-5 text-center text-lg-start" style="aqua-color: hsl(0, 0%, 0%)">
    <div class="container">
      <div class="row gx-lg-5 align-items-center">
          <center>
        <div class="col-lg-8 mb-1 mb-lg-5">
          <h1 class="my-10 display-8 fw-bold ls-tight">
          <br />
            <span class="text-primary">LDBAJ</span> <br><br>
            <span class="text-primary">LIGA BARRIAL  </span>
                          <td style="padding:2%;">
                <a>
              <img class="img-responsive d-block w-100" style="width:10%; height:180px" alt="Scars In Heaven (Official Video)" src="https://www.wikihow.com/images/1/16/Play-Soccer-Step-10-Version-8.jpg" />
            </a>
                </td>
              </tr>

          </h1>
        </div>
  </center>
  <center>
        <div class="col-lg-6 mb-6 mb-lg-0">
          <div class="card">
            <div class="card-body py-5 px-md-5">
              <form>
                <!-- 2 column grid layout with text inputs for the first and last names -->

                <!-- Email input -->
                <div class="form-outline mb-4">
                  <input type="email" name="email_usu_aj" id="email_usu_aj" value="" placeholder="INGRESE SU CORREO" class="form-control" />
                  <label class="form-label" for="form3Example3">CORREO ELECTRONICO</label>
                    <span class="help-block">Ej. rene@gmail.com</span>

                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                <input type="password" name="password_usu_aj" id="password_usu_aj" value="" placeholder="INGRESE SU CONTRASEÑA" class="form-control"required>
                  <label class="form-label" for="form3Example4">CONTRASEÑA</label>
                </div>

                <!-- Checkbox -->


                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">
                  INGRESAR
                </button>

                <!-- Register buttons -->

              </form>
            </div>
          </div>
        </div>
        </center>
      </div>
    </div>
  </div>
  <!-- Jumbotron -->
</section>
