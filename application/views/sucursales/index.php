

<center> <h3>gestion de sucursales </h3></center>

<?php if ($listadosucursal): ?>
  <table id="tbl-sucursal" class="table table-stripe table-border table-hover">
<thead>
  <th class="text-center">Id</th>
  <th class="text-center">provincia</th>
  <th class="text-center">ciudad</th>
  <th class="text-center">estado</th>
  <th class="text-center">direccion</th>
  <th class="text-center">Email</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadosucursal-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_suc_aj ; ?></td>
      <td class="text-center"><?php echo $temporal->provincia_aj ; ?></td>
      <td class="text-center"><?php echo $temporal->ciudad_aj ; ?></td>
      <td class="text-center"><?php echo $temporal->estado_aj ; ?></td>
      <td class="text-center"><?php echo $temporal->direccion_aj ; ?></td>
      <td class="text-center"><?php echo $temporal->email_aj ; ?></td>
      <td>
            <a href="<?php echo site_url("sucursales/editar"); ?>/<?php echo $temporal->id_suc_aj; ?>" class="btn btn-warning"> <i class="glyphicon glyphicon-trash"> Editar</i></a>
        <a href="<?php echo site_url("sucursales/borrar"); ?>/<?php echo $temporal->id_suc_aj; ?>" class="btn btn-danger"> <i class="glyphicon glyphicon-trash"> Eliminar</i></a>

      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<br>
<center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_sucursal">Nuevo Sucursal</button></center>

<!-- Modal -->
<div id="modal_sucursal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva Sucursal</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_producto" class="" action="<?php echo site_url('sucursales/guardar'); ?>" method="post">
          <b>Provincia:</b>
          <br>
          <input type="text" id="provincia_aj " name="provincia_aj" value="" placeholder="ingrese el nombre del provincia" class="form-control">
          <br>

          <b>Ciudad:</b>
          <br>
          <input type="text" id="ciudad_aj " name="ciudad_aj" value="" placeholder="ingrese la ciudad" class="form-control">
          <br>

          <b>Estado:</b>
          <br>
          <input type="text" id="estado_aj" name="estado_aj" value="" placeholder="ingrese el Estado" class="form-control">
          <br>
          <b>Dirreccion:</b>
          <br>
          <input type="text" id="direccion_aj" name="direccion_aj" value="" placeholder="ingrese el Direccion" class="form-control">
          <br>
          <b>Email:</b>
          <br>
          <input type="text" id="email_aj" name="email_aj" value="" placeholder="ingrese el Email" class="form-control">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_sucursal").validate({
   rules:{
       provincia_aj :{
         required:true
       },
       ciudad_aj :{
         required: true
       },
       estado_aj :{
         required: true
       },
       direccion_aj :{
         required: true
       }
       email_aj :{
         required: true
       }
   },
   messages:{
     provincia_aj :{
       required: "Ingrese su provincia"
     },
     ciudad_aj :{
       required: "Ingrese su ciudad"
     },
     estado_aj :{
       required: "Ingrese su estado"
     },
     direccion_aj :{
       required: "Ingrese su dirreccion"
     }
     email_aj :{
       required: "Ingrese su email"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("sucursales/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_sucursal").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_producto").modal("hide");
       }
     });
   }
});
</script>

<script type="text/javascript">
  $('#tbl-sucursal').DataTable();
</script>
