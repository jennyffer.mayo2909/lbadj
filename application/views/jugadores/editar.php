<center>
<form class="" action="<?php echo site_url();?>/jugadores/procesarActualizacion" method="post" >
  <div style="border:3px ; width:40%; margin-top:4px; margin-left:7%; margin-right:7%;">

    <h4> <b>ACTUALIZAR DATOS DE JUGADORES</b> </h4> <br>
    <input type="hidden" name="id_jug_aj" id="id_jug_aj" value="<?php echo $jugador->id_jug_aj; ?>">
    <br>
    <label for="">EQUIPO</label>
    <select  name="fk_id_equi_aj" id="fk_id_equi_aj" required>
      <option value="">--Seleccione un equipo--</option>
      <?php if ($listadoEquipos): ?>
        <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
          <option value="<?php echo $equipoTemporal->id_equi_aj; ?>">
            <?php echo $equipoTemporal->nombre_equi_aj; ?>
          </option>
        <?php endforeach; ?>
      <?php endif; ?>
    </select>
    <br>
    <label for="">Nombre: </label>
    <input class="form-control" value="<?php echo $jugador->nombre_jug_aj; ?>" required="requiered" onkeypress="return validar(event)"  type="text" name="nombre_jug_aj" id='nombre_jug_aj' value="" placeholder="Ingrese el nombre">
    <br>
    <label for="">Apellido</label>
    <br>
    <input class="form-control" value="<?php echo $jugador->apellido_jug_aj; ?>" required="requiered" onkeypress="return validar(event)" type="text" name="apellido_jug_aj" id="apellido_jug_aj" value="" placeholder="Ingrese el apellido">
    <br>
    <label for="">Identificacion</label> <br>
    <input class="form-control" value="<?php echo $jugador->identificacion_jug_aj; ?>" required="requiered"  type="number" name="identificacion_jug_aj" id="identificacion_jug_aj" value="" placeholder="Ingrese su identificacion">
    <br>
    <label for="">Numero</label> <br>
    <input class="form-control" value="<?php echo $jugador->numero_jug_aj; ?>" required="requiered"  type="number" name="numero_jug_aj" id="numero_jug_aj" value="" placeholder="Ingrese numero de camiseta">
    <br>
    <label for="" >Estado: </label>
    <select   required="requiered"  name="estado_jug_aj" id=estado_jug_aj value="" placeholder="Seleccione su estado">
      <option value="">--Seleccione--</option>
      <option value="NACIONAL" >NACIONAL</option>
      <option value="FORANEO">FORANEO</option>
    </select>
    <br>
    <label for="">GOLES: </label>
    <br>
    <input class="form-control" value="<?php echo $jugador->goles_jug_aj; ?>" required="requiered" type="number" name="goles_jug_aj" id='goles_jug_aj' value="" placeholder="Ingrese el número de teléfono">
    <br>
    <label for="" >PERFIL: </label>
    <select  required="requiered"  name="perfil_jug_aj" id=perfil_jug_aj value="" placeholder="Seleccione su estado">
      <option value="">--Seleccione--</option>
      <option value="USUARIO" >USUARIO</option>
      <option value="DELEGADO">DELEGADO</option>
      <option value="JUGADOR">JUGADOR</option>
    </select>
    <label for="">Email: </label>
    <br>
    <input class="form-control" value="<?php echo $jugador->email_jug_aj; ?>" required="requiered" type="text" name="email_jug" id='email_jug' value="" placeholder="Ingrese el correo electrónico">
    <br>
    <label for="">CONTRASEÑA: </label>
    <br>
    <input class="form-control" value="<?php echo $jugador->password_jug_aj; ?>" required="requiered" onkeypress="return validar(event)" type="text" name="password_jug" id='password_jug' value="" placeholder="actualice la contraseña">
    <br><br>
  <button type="submit" name="button" class="btn btn-primary">ACTUALIZAR</button>
    <a href="<?php echo site_url(); ?>/jugadores/index"   class="btn btn-warning"> <i class="fa fa-times"></i>      CANCELAR    </a>
</div>
</form>
</center>


<!-- ************************************ACTIVANDO EL equipo SELECCIONADO POR EL JUGADOR*********************************** -->


<script type="text/javascript">
    $("#fk_id_equi_aj").val("<?php echo $jugador->fk_id_equi_aj; ?>");
    $("#estado_jug_aj").val("<?php echo $jugador->estado_jug_aj; ?>");
    $("#perfil_jug_aj").val("<?php echo $jugador->perfil_jug_aj; ?>");
</script>
