<center>
<form class="" action="<?php echo site_url();?>/jugadores/guardarJugador" method="post" id="frm_nuevo_jugador">
  <div style="border:3px ; width:40%; margin-top:4px; margin-left:7%; margin-right:7%;" type="table">
    <h4> <b>AGREGAR JUGADOR</b> </h4>
     <br>
    <label for="">EQUIPO</label>
    <br>
    <select   name="fk_id_equi_aj" id="fk_id_equi_aj" required>
      <option value="">--Seleccione un equipo--</option>
      <?php if ($listadoEquipos): ?>
        <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
          <option value="<?php echo $equipoTemporal->id_equi_aj; ?>">
            <?php echo $equipoTemporal->nombre_equi_aj; ?>
          </option>
        <?php endforeach; ?>
      <?php endif; ?>
    </select>
    <br>
    <br>
    <label for="">Nombre: </label>
    <input class="form-control"  required="requiered" onkeypress="return validar(event)"  type="text" name="nombre_jug_aj" id='nombre_jug_aj' value="" placeholder="Ingrese el nombre">
    <br>
    <label for="">Apellido</label>
    <br>
    <input class="form-control" required="requiered" onkeypress="return validar(event)" type="text" name="apellido_jug_aj" id="apellido_jug_aj" value="" placeholder="Ingrese el apellido">
    <br>
    <label for="">Identificacion</label> <br>
    <input class="form-control"  required="requiered"  type="number" name="identificacion_jug_aj" id="identificacion_jug_aj" value="" placeholder="Ingrese su identificacion">
    <br>
    <label for="">Numero</label>
    <br>
    <input class="form-control"  required="requiered"  type="number" name="numero_jug_aj" id="numero_jug_aj" value="" placeholder="Ingrese numero de camiseta">
    <br>
    <label for="" >Estado: </label>
    <br>
    <select  type="text" required="requiered"  name="estado_jug_aj" id=estado_jug_aj value="" placeholder="Seleccione su estado">
      <option value="">--Seleccione--</option>
      <option value="NACIONAL" >NACIONAL</option>
      <option value="FORANEO">FORANEO</option>
    </select>
    <br>
    <label for="">GOLES: </label>
    <br>
    <input class="form-control"  required="requiered" type="number" name="goles_jug_aj" id='goles_jug_aj' value="" placeholder="Ingrese el número de goles">
    <br>
    <label for="" >PERFIL: </label>
    <br>
    <select type="text" required="requiered"  name="perfil_jug_aj" id=perfil_jug_aj value="" placeholder="Seleccione su perfil">
      <option value="">--Seleccione--</option>
      <option value="ADMINISTRADOR" >ADMINISTRADOR</option>
      <option value="DELEGADO">DELEGADO</option>
      <option value="JUGADOR">JUGADOR</option>
    </select>
    <br>
    <label for="">EMAIL</label> <br>
    <input class="form-control"  required="requiered"  type="email" name="email__jug_aj" id="email_jug_aj" value="" placeholder="Ingrese su email">
    <br>
    <label for="">CONTRASEÑA: </label>
    <br>
    <input class="form-control" required="requiered" onkeypress="return validar(event)" type="number" name="password_jug_aj" id='password_jug_aj' value="" placeholder="ingrese la contraseña">
    <br>
    <br>
  <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    <a href="<?php echo site_url(); ?>/jugadores/index"
      class="btn btn-warning"><i class="fa fa-times"></i>
      CANCELAR
    </a>
    <center>
</div>
</form>
</center>


<!-- AGREGAMOS TAG PARA LLAMAR AL FORMULARIO -->
<script type="text/javascript">
    $("#frm_nuevo_jugador").validate({
      rules:{
        fk_id_equi_aj:{
          required:true
        },

        apellido_jug_aj:{
          letras:true,
          required:true
        },
        nombre_jug_aj:{
          letras:true,
          required:true
        },
        identificacion_jug_aj:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        numero_jug_aj:{
          required:true,
          minlength:1,
          maxlength:3,
          digits:true
        },
        estado_jug_aj:{
          required:true
        },
        goles_jug_aj:{
          required:true
        },
        perfil_jug_aj:{
          required:true
        },
        email_jug_aj:{
          letras:true,
          required:true
        },
        password_jug_aj:{
          required:true,
          minlength:6
        }
      },

      messages:{
        fk_id_pais_aj:{
          required:"Por favor seleccione el pais"
        },
        nombre_jug_aj:{
          letras:"El nombre solo debe tener letras",
          required:"Por favor ingrese el nombre"
        },
        apellido_jug_aj:{
          letras:"El apellido solo debe tener letras",
          required:"Por favor ingrese el apellido"
        },
        identificacion_jug_aj:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La camiseta solo acepta números"
        },
        numero_jug_aj:{
          required:"Por favor ingrese el número de teléfono celular",
          minlength:"El número de camiseta debe tener mínimo 1 digito",
          maxlength:"El número de celular debe tener máximo 3 digitos",
          digits:"El número de celular solo acepta números"
        },
        estado_jug_aj:{
          required:"Por favor seleccione el estado"
        },
        perfil_jug_aj:{
          required:"Por favor seleccione un perfil"
        },
        email_jug_aj:{
          letras:"email Incorrecto",
          required:"Por favor ingrese nuevamente"
        },
        password_jug_aj:{
          required:"Por favor introduzca una contraseña"
        }

      },
    });
</script>
