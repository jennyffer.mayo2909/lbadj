<legend class="text-center">
  <i class="glyphicon glyphicon-plus"></i>
  AGREGAR NUEVO
</legend>
<form id="frm_nuevo_cuartos"class=""
enctype="multipart/form-data"
action="<?php echo site_url('cuartos/guardarCuartos'); ?>" method="post">

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Fotografia :</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_cu_ja" name="foto_cu_ja" value="required" accept="image/*"class="form-control"placeholder=" seleccione una foto "  required>
  </div>
</div>

<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">Pais:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="pais_cu_ja" name="pais_cu_ja" value=""class="form-control"placeholder="Ingrese el nombre del pais  "  required >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GRUPO :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="grupo_cu_ja" name="grupo_cu_ja" value=""class="form-control"placeholder="Ingrese el grupo" required>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES   :</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="goles_cu_ja" name="goles_cu_ja" value=""class="form-control"placeholder="Ingrese los goles en contra  " required>
  </div>
</div>

<br>
<br>
<div class="row">
  <div class="col-md-4">
  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">
<i class="glyphicon glyphicon-ok"></i>
Guardar
    </button>
    <a href="<?php echo site_url('cuartos/index'); ?>" class="btn btn-danger">
<i class="glyphicon glyphicon-remove"></i>
Cancelar
    </a>
  </div>

</div>

</form>
<script type="text/javascript">
     $("#frm_nuevo_cuartos").validate({
        rules:{

            pais_cu_ja:{
              letras:true,
        required:true
            },
            grupo_cu_ja:{
              letras:true,
              digits:true
            },
            goles_cu_ja:{
              required:true,
                digits:true
          }
            },
          messages:{


              pais_cu_ja:{
                letras:"pais Incorrecto",
          required:"Por favor ingrese el pais"
              },
            grupo_cu_ja:{
                required:"Por favor ingrese el ranking global",


              },
              goles_cu_ja:{
                  required:"Por favor ingrese el ranking global",


                }


            },
         });
    </script>
