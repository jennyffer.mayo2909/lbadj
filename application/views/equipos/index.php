<script type="text/javascript">
    $("#menu-equipos").addClass('active');
</script>
<br>
<center>
  <h2>REGISTRO DE EQUIPOS</h2>

</center>
      <center>
<a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-success">
<i class="glyphicon glyphicon-plus"></i>
Agregar nuevo
    </a>
  </center>

<?php if ($listadoEquipos): ?>
  <table class="table table-bordered table-striped table-hover" id="tbl-clientes_aj">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">CATEGORIA</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoEquipos->result() as $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_equi_aj;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_equi_aj;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->categoria_equi_aj;?>
        </td>
        <td class="text-center">
          <a href="<?php echo site_url(); ?>/equipos/editar/<?php echo $filaTemporal->id_equi_aj;?>" class="btn btn-warning">Editar</a>
          <a href="javascript:void(0)"
                              onclick="procesarEliminacion('<?php echo $filaTemporal->id_equi_aj; ?>');"
                              class="btn btn-danger">
                               <i class="fa fa-trash"></i>
                             </a>
        </td>
      </tr>

      <?php endforeach; ?>

    </tbody>

    <tfoot>

    </tfoot>
  </table>

<?php else: ?>
  <div class="alert alert-danger">
    <h3>No se encontraron equipos por el momento</h3>

  </div>

<?php endif; ?>
<script type="text/javascript">
    function procesarEliminacion(id_equi_aj){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el equipo de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/equipos/procesarEliminacion/"+id_equi;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
  $("#tbl-clientes_aj").DataTable({
    responsive: true,
 autoWidth: false,
 position: 'center',
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
    language: {
              url: "https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-MX.json"
          }
  });
</script>
