<br>
<center>
  <h2>REGISTRO DE ARBITRO</h2>

</center>
<br>
<center>
    <a href="<?php echo site_url(); ?>/arbitros/nuevo" class="btn btn-secondary">
    AGREGAR ARBITRO
    </a>
    <br>
    <br>
</center>

<?php if ($listadoArbitros): ?>
  <table class="table table-bordered table-striped table-hover" id="frm_arbitro">

    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">PASSWORD</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($listadoArbitros->result() as $filaTemporal): ?>

      <tr>
        <td class="text-center">
          <?php echo $filaTemporal->id_arb_aj;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->nombre_arb_aj;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->apellido_arb_aj;?>
        </td>
        <td class="text-center">
          <?php echo $filaTemporal->email_aj;?>
        </td>
        <td class="text-center">
          <?php echo base64_encode($filaTemporal->password_arb_aj);?>
        </td>

        <center>
        <td class="text-center">
          <a href="<?php echo site_url(); ?>/arbitros/editar/<?php echo $filaTemporal->id_arb_aj;?>" class="btn btn-warning" ><i class="fa fa-pencil"></i></a>

          <a href="javascript:void(0)"
                              onclick="procesarEliminacion('<?php echo $filaTemporal->id_arb_aj; ?>');"
                              class="btn btn-danger">
                               <i class="fa fa-trash"></i>
                             </a>

        </td>
      </center>
      <?php endforeach; ?>

    </tbody>

    <tfoot>

    </tfoot>
  </table>

<?php else: ?>
  <div class="alert alert-damger">
    <h3>NO SE ENCONTRARON JUEGOS REGISTRADOS</h3>
  </div>
  <?php endif; ?>
<script type="text/javascript">
    function procesarEliminacion(id_arb_aj){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el arbitro de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/arbitros/procesarEliminacion/"+id_arb_aj;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
  $("#frm_arbitro").DataTable({
    responsive: true,
 autoWidth: false,
 position: 'center',
    dom: 'Blfrtip',

    ],
    language: {
              url: "https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-MX.json"
          }
  });
</script>
