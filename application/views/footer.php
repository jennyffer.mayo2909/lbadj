
    <footer class="footer-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <div class="widget mb-3">
              <h3>PAGINA ESTABLECIDA POR </h3>
  <center>
<h5>DESARROLLADO POR:</h5>
<h5>Jennyffer Mayo</h5>
<h5>Alex Iza</h5>
</center>

            </div>
          </div>



          <div class="col-lg-3">
            <div class="widget mb-3">
              <h3>Social</h3>
              <ul class="list-unstyled links">
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Instagram</a></li>
                <li><a href="#">Youtube</a></li>
              </ul>
            </div>
          </div>

          <div class="col-lg-3">
            <div class="widget mb-3">
              <center>
            <img src="https://ligasanbuenaventura.ec/images/LogoPrincipal.png" alt="" style="borde-radius:300px; height:300px; width:380px;">
          </center>

              <ul class="list-unstyled links">

              </ul>
            </div>
          </div>


        </div>


      </div>



        <div class="row text-center">
          <div class="col-md-12">
            <div class=" pt-5">
              <p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;
                <script>
                  document.write(new Date().getFullYear());
                </script> All rights reserved | This template is made with <i class="icon-heart"
                  aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              </p>
            </div>
          </div>

        </div>
      </div>
    </footer>



  </div>
  <!-- .site-wrap -->

  <script src="<?php echo base_url(); ?>/assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery.stellar.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery.countdown.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/aos.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery.sticky.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/jquery.mb.YTPlayer.min.js"></script>


  <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>

</body>

</html>
<?php if ($this->session->flashdata('confirmacion')): ?>
      <script type="text/javascript">
      iziToast.success({
      title: 'CONFIRMACIÒN',
      message: '<?php echo $this->session->flashdata('confirmacion') ?> ',
      position: 'topRight',
    });
      </script>
    <?php endif; ?>
    <?php if ($this->session->flashdata('error')): ?>
    <script type="text/javascript">
    iziToast.error({
    title: 'ERROR',
    message: '<?php echo $this->session->flashdata('error') ?> ',
    position: 'topRight',
  });
    </script>
  <?php endif; ?>


  <?php if ($this->session->flashdata("bienvenida")): ?>
    <script type="text/javascript">
      iziToast.info({
           title: 'CONFIRMACIÓN',
           message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
           position: 'topRight',
         });
    </script>
  <?php endif; ?>
  <?php if ($this->session->flashdata("ok")): ?>
    <script type="text/javascript">
      iziToast.info({
           title: 'CONFIRMACIÓN',
           message: '<?php echo $this->session->flashdata("ok"); ?>',
           position: 'topRight',
         });
    </script>
  <?php endif; ?>

  <script type="text/javascript">
          function confimacionCerrarSesion(id_usu){
                iziToast.question({
                    timeout: 20000,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'CONFIRMACIÓN',
                    message: '¿Esta seguro que de sea Cerrar Sesion?',
                    position: 'center',
                    buttons: [
                        ['<button><b>SI</b></button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href=
                            ""+id_usu;

                        }, true],
                        ['<button>NO</button>', function (instance, toast) {

                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                        }],
                    ]
                });
          }
      </script>
