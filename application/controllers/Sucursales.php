<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sucursales extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("sucursal");
				}
				//renderiza la vista index de estudiantes

	// InvalidArgumentException
  public function index()
	{
		$data["listadosucursal"]=$this->sucursal->obtenerTodos();
    $this->load->view("header");
    $this->load->view("sucursales/index",$data);
    $this->load->view("footer");
  }
// asignaturas



	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar(){
		$datosSucursal=array(
	    "provincia_aj"=>$this->input->post('provincia_aj'),
	    "ciudad_aj"=>$this->input->post('ciudad_aj'),
	    "estado_aj"=>$this->input->post('estado_aj'),
      "direccion_aj"=>$this->input->post('direccion_aj'),
	    "email_aj"=>$this->input->post('email_aj')
		);
		if($this->sucursal->insertar($datosSucursal)){
			redirect('sucursales/index');
		}else{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

//borrar profesor
	public function borrar($id_suc_aj )
	{
		if($this->sucursal->eliminarPorId($id_suc_aj )){
			$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
		}else{
			$resultado=array("estado"=>"error");
		}
		redirect('sucursales/index');
	}
	public function editar($id)
	{
		$data["sucursalEditar"]=$this->sucursal->ObtenerPorId($id);
			$this->load->view('header');
			$this->load->view('sucursales/editar',$data);
			$this->load->view('footer');
	}
	public function sucursalEditar()
	{
			$datosSucursalEditado=array(
        "provincia_aj"=>$this->input->post('provincia_aj'),
  	    "ciudad_aj"=>$this->input->post('ciudad_aj'),
  	    "estado_aj"=>$this->input->post('estado_aj'),
        "direccion_aj"=>$this->input->post('direccion_aj'),
  	    "email_aj"=>$this->input->post('email_aj')
			);
	$id=$this->input->post("id_suc_aj");

	if($this->sucursal->actualizar($id,$datosSucursalEditado)) {
	redirect('sucursales/index');
	}else {
		echo "Error";
}
}





}//cierre de la clase no borrar despues estas en problemas te conozco
