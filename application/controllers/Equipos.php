
<?php
    class Equipos extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("equipo");

      }

      public function index(){
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("equipos/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $this->load-> view("header");
        $this->load-> view("equipos/nuevo");
        $this->load-> view("footer");
      }
      public function editar($id_equi_aj){
        $data["listadoEquipos"]=$this->equipo->consultarPorId($id_equi_aj);
        $this->load-> view("header");
        $this->load-> view("equipos/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_equi_aj=$this->input->post("id_equi_aj");
        $datosEquipoEditado=array(
            "nombre_equi_aj"=>$this->input->post("nombre_equi_aj"),
            "categoria_equi_aj"=>$this->input->post("categoria_equi_aj")
          );

        if ($this->equipo->actualizar($id_equi_aj,$datosEquipoEditado)) {
          // echo "INSERCION EXITOSA";
            $this->session->set_flashdata("confirmacion","Equipo actualizado exitosamente.");

        }
        else {
          $this->session->set_flashdata("error","Error al procesar, intente nuevvamente");

        }
          redirect("equipos/index");
      }


      public function guardarEquipo(){
        $datosNuevoEquipo=array(
          "nombre_equi_aj"=>$this->input->post("nombre_equi_aj"),
          "categoria_equi_aj"=>$this->input->post("categoria_equi_aj")
        );
      if ($this->equipo->insertar($datosNuevoEquipo)) {
        $this->session->set_flashdata("confirmacion","Equipo insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevvamente");
      }
        redirect("equipos/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_equi_aj){

        if($this->arbitro->eliminar($id_equi_aj)){
          $this->session->set_flashdata("confirmacion","Calendario eliminado exitosamente.");
                  redirect("arbitros/index");
        }else{
            $this->session->set_flashdata("error al eliminar exitosamente.");
        }
        redirect("equipos/index");
    }
} //Cierre de la clase
?>
