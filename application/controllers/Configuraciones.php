configuraciones<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("configuracion");
				}
				//renderiza la vista index de estudiantes

	// InvalidArgumentException
  public function index()
	{
		$data["listadoConfiguracion"]=$this->configuracion->obtenerTodos();
    $this->load->view("header");
    $this->load->view("configuraciones/index",$data);
    $this->load->view("footer");
  }
// asignaturas

	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar()
	{
		$datosConfiguracion=array(
	    "nombre_empresa_aj"=>$this->input->post('nombre_empresa_aj'),
	    "ruc_aj"=>$this->input->post('ruc_aj'),
	    "telefono_aj"=>$this->input->post('telefono_aj'),
	    "dirreccion_aj"=>$this->input->post('dirreccion_aj'),
	    "representante_aj "=>$this->input->post('representante_aj')
		);
		if($this->configuracion->insertar($datosConfiguracion)){
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}else{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

//borrar profesor
	public function borrar($id_con_aj)
	{
		if($this->configuracion->eliminarPorId($id_con_aj)){
			$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
		}else{
			$resultado=array("estado"=>"error");
		}
		redirect('configuraciones/index');
	}
	public function editar($id)
	{
		$data["configuracionEditar"]=$this->configuracion->ObtenerPorId($id);
			$this->load->view('header');
			$this->load->view('configuraciones/editar',$data);
			$this->load->view('footer');
	}
	public function configuracionEditar()
	{
			$datosConfiguracionEditado=array(
		    "id_con_aj"=>$this->input->post('id_con_aj'),
  	    "nombre_empresa_aj"=>$this->input->post('nombre_empresa_aj'),
  	    "ruc_aj"=>$this->input->post('ruc_aj'),
  	    "telefono_aj"=>$this->input->post('telefono_aj'),
  	    "dirreccion_aj"=>$this->input->post('dirreccion_aj'),
  	    "representante_aj "=>$this->input->post('representante_aj')
			);
	$id=$this->input->post("id_con_aj");

	if($this->configuracion->actualizar($id,$datosConfiguracionEditado)) {
	redirect('configuraciones/index');
	}else {
		echo "Error";
}
}





}//cierre de la clase no borrar despues estas en problemas te conozco
