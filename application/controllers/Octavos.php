<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Octavos extends CI_Controller {
	public function __construct(){
		parent::__construct();

	
		$this->load->model("octavo");
	}
// renderiza la vista estudiantes
	public function index()
	{

		$data["listadoOctavos"]=$this->octavo->obtenerTodos();
		$this->load->view('header');

		$this->load->view('octavos/index',$data);
		$this->load->view('footer');
	}
// renderiza la vista de nuevo
  	public function nuevo()
  	{

  		$this->load->view('header');

  		$this->load->view('octavos/nuevo');
  		$this->load->view('footer');
  	}
		// funcion para capturar los datos del formulario
		public function guardarOctavo(){
        $datosNuevoOctavo=array(


          "foto_oc_ja"=>$this->input->post("foto_oc_ja"),
          "pais_oc_ja"=>$this->input->post("pais_oc_ja"),
					"grupo_oc_ja"=>$this->input->post("grupo_oc_ja"),
        	"goles_oc_ja"=>$this->input->post("goles_oc_ja")

        );

				// INICIO DE PROCESO DE SUBIDA DE FOTOGRAFIA
				$this->load->library("upload");// activando la liibreria de subida de archivos
		    $new_name = "foto_" . time() . "_" . rand(1, 5000);//generando un nombre aleatorio
		    $config['file_name'] = $new_name;

		    $config['upload_path'] = FCPATH . 'uploads/octavos/';// ruta de subida
		    $config['allowed_types'] = 'jpg|png';//pdf|docx
		    $config['max_size']  = 5*1024;//5MB
		    $this->upload->initialize($config);// inicializar la configuracion
				// validando la subida del aarchivo
		    if ($this->upload->do_upload("foto_oc_ja")) {
		      $dataSubida = $this->upload->data();
		      $datosNuevoOctavo["foto_oc_ja"] = $dataSubida['file_name'];
		    }
				// FIN DEL PROCESO DE ARCHIVOS


				print_r($datosNuevoOctavo);
				if ($this->octavo->insertar($datosNuevoOctavo)) {
									 				$this->session->set_flashdata('confirmacion','insertado exitosamente');
									 				}else	{
									 			$this->session->set_flashdata('error','verifique e intente de nuevo');
									 				}
									 					redirect('octavos/index');
									       }


	public function borrar($id_oc_ja){

					if($this->octavo->eliminarPorId($id_oc_ja)){
						$this->session->set_flashdata('confirmacion','estudiante eliminado exitosamente');
	            }else{
	            $this->session->set_flashdata('error','verifique e intente nuevamnete ');
	            }
							redirect('octavos/index');
	        }

			//funcion para renderizar eliminar estudiantes
			// ACTUALIZAR
			public function actualizar ($id){
				$data["octavosEditar"]=$this->octavo->obtenerporId($id);
				$this->load->view("header");
				$this->load->view("octavos/actualizar",$data);
				$this->load->view("footer");
			}

			public function procesarActualizacion(){
			$id_oc_ja=$this->input->post("id_oc_ja");
			$datosOctavoEditado=array(
				"foto_oc_ja"=>$this->input->post("foto_oc_ja"),
				"pais_oc_ja"=>$this->input->post("pais_oc_ja"),
				"grupo_oc_ja"=>$this->input->post("grupo_oc_ja"),
				"goles_oc_ja"=>$this->input->post("goles_oc_ja")
			);
			if($this->octavo->actualizar($id_oc_ja,$datosOctavoEditado)){
			}else{

			}
			redirect('octavos/index');

		}
}
?>
