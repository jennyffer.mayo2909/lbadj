
<?php
    class Jugadores extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("jugador");
        $this->load->model("equipo");
      }

      public function index(){
        $data["listadoJugadores"]=$this->jugador->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("jugadores/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("jugadores/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_jug_aj){
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $data["jugador"]=$this->jugador->consultarPorId($id_jug_aj);
        $this->load-> view("header");
        $this->load-> view("jugadores/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_jug_aj=$this->input->post("id_jug_aj");
        $datosJugadorEditado=array(
            "nombre_jug_aj"=>$this->input->post("nombre_jug_aj"),
            "apellido_jug_aj"=>$this->input->post("apellido_jug_aj"),
            "identificacion_jug_aj"=>$this->input->post("identificacion_jug_aj"),
            "numero_jug_aj"=>$this->input->post("numero_jug_aj"),
            "estado_jug_aj"=>$this->input->post("estado_jug_aj"),
            "goles_jug_aj"=>$this->input->post("goles_jug_aj"),
            "perfil_jug_aj"=>$this->input->post("perfil_jug_aj"),
            "email_jug_aj"=>$this->input->post("email_jug_aj"),
            "password_jug_aj"=>$this->input->post("password_jug_aj"),
            "fk_id_equi_aj"=>$this->input->post("fk_id_equi_aj")
          );
        if ($this->jugador->actualizar($id_jug_aj,$datosJugadorEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("jugadores/index");
        }
        else {
          echo "ERROR AL INSERTAR";
        }
      }

//-
      public function guardarJugador(){
        $datosNuevoJugador=array(
          "nombre_jug_aj"=>$this->input->post("nombre_jug_aj"),
          "apellido_jug_aj"=>$this->input->post("apellido_jug_aj"),
          "identificacion_jug_aj"=>$this->input->post("identificacion_jug_aj"),
          "numero_jug_aj"=>$this->input->post("numero_jug_aj"),
          "estado_jug_aj"=>$this->input->post("estado_jug_aj"),
          "goles_jug_aj"=>$this->input->post("goles_jug_aj"),
          "perfil_jug_aj"=>$this->input->post("perfil_jug_aj"),
          "email_jug_aj"=>$this->input->post("email_jug_aj"),
          "password_jug_aj"=>$this->input->post("password_jug_aj"),
          "fk_id_equi_aj"=>$this->input->post("fk_id_equi_aj")
        );

      if ($this->jugador->insertar($datosNuevoJugador)) {
        $this->session->set_flashdata("confirmacion","Jugador insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente");
      }
        redirect("jugadores/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_jug_aj){
      if($this->jugador->eliminar($id_jug_aj)){
        redirect("jugadores/index");
      }else{
        echo "ERROR AL ELIMINAR";
      }
    }
} //Cierre de la clase
?>
