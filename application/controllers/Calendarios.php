
<?php
    class Calendarios extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("calendario");
        $this->load->model("equipo");

      }

      public function index(){
        $data["listadoCalendarios"]=$this->calendario->consultarTodos();

        $this->load-> view("header");
        $this->load-> view("calendarios/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoCalendarios"]=$this->calendario->consultarTodos();
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("calendarios/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_cal_aj){
        $data["listadoCalendarios"]=$this->calendario->consultarPorId($id_cal_aj);
        $data["listadoEquipos"]=$this->equipo->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("calendarios/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_cal_aj=$this->input->post("id_cal_aj");
        $datosCalendarioEditado=array(
            "fk_id_equipo1_aj"=>$this->input->post("fk_id_equipo1_aj"),
            "fk_id_equipo2_aj"=>$this->input->post("fk_id_equipo2_aj"),
            "fecha_hora_aj"=>$this->input->post("fecha_hora_aj"),
            "resultado_aj"=>$this->input->post("resultado_aj"),
            "informe_aj"=>$this->input->post("informe_aj")
          );

        if ($this->calendario->actualizar($id_cal_aj,$datosCalendarioEditado)) {
          // echo "INSERCION EXITOSA";
          $this->session->set_flashdata("confirmacion","Calendario actualizado exitosamente.");

        }
        else {
          $this->session->set_flashdata("error","Error al procesar, intente nuevvamente");
        }
        redirect("calendarios/index");
      }


      public function guardarCalendario(){
        $datosNuevoCalendario=array(
          "fk_id_equipo1_aj"=>$this->input->post("fk_id_equipo1_aj"),
          "fk_id_equipo2_aj"=>$this->input->post("fk_id_equipo2_aj"),
          "fecha_hora_aj"=>$this->input->post("fecha_hora_aj"),
          "resultado_aj"=>$this->input->post("resultado_aj"),
          "informe_aj"=>$this->input->post("informe_aj")
        );
      if ($this->calendario->insertar($datosNuevoCalendario)) {
        $this->session->set_flashdata("confirmacion","Calendario insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevvamente");
      }
        redirect("calendarios/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_cal_aj){

        if($this->calendario->eliminar($id_cal_aj)){
          $this->session->set_flashdata("confirmacion","Calendario eliminado exitosamente.");
                  redirect("calendarios/index");
        }else{
            $this->session->set_flashdata("error al eliminar exitosamente.");
        }
        redirect("calendarios/index");
    }
} //Cierre de la clase
?>
