<?php
      class Usuarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");
            //validando si alguien esta conectado
            if ($this->session->userdata("c0nectado@j")) {
              // si esta conectado
              if ($this->session->userdata("c0nectado@j")->perfil_usu=="ADMINISTRADOR")
              {
                //si es ADMINISTRADOR
              }else{
                redirect("/");
              }
            } else {
              redirect("seguridades/formularioLogin");
            }
        }
        public function index(){
          $this->load->view("header");
          $this->load->view("usuarios/index");
          $this->load->view("footer");
        }
        public function listado(){
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
          $this->load->view("usuarios/listado",$data);
        }
        //inserccion
        public function insertarUsuario(){
        $data=array(
    "apellido_usu_aj"=>$this->input->post("apellido_usu_aj"),
    "nombre_usu_aj"=>$this->input->post("nombre_usu_aj"),
    "email_usu_aj"=>$this->input->post("email_usu_aj"),
    "password_usu_aj"=>md5($this->input->post("password_usu_aj")),
    "perfil_usu_aj"=>$this->input->post("perfil_usu_aj")
        );
        if($this->usuario->insertarUsuario($data)){
          echo json_encode(array("respuesta"=>"ok"));
        }else{
          echo json_encode(array("respuesta"=>"error"));

        }
      }
      public function eliminarUsuario(){
        $ide_usu_aj=$this->input->post("ide_usu_aj");
        if ($this->usuario->eliminar($ide_usu_aj)) {
          echo json_encode(array("respuesta"=>"ok"));
        } else{
          echo json_encode(array("respuesta"=>"error"));
        }
      }


}//cierre de la clase
   ?>
