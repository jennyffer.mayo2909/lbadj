
<?php
    class Arbitros extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("arbitro");

      }

      public function index(){
          $data["listadoArbitros"]=$this->arbitro->consultarTodos();
          $this->load-> view("header");
          $this->load-> view("arbitros/index",$data);
          $this->load-> view("footer");
        }

      public function nuevo(){
        $this->load-> view("header");
        $this->load-> view("arbitros/nuevo");
        $this->load-> view("footer");
      }




      public function editar($id_arb_aj){
        //$data["listadoCalendarios"]=$this->calendario->consultarPorId($id_cal);
        $data["listadoArbitros"]=$this->arbitro->consultarPorId($id_arb_aj);
        $this->load-> view("header");
        $this->load-> view("arbitros/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_arb_aj=$this->input->post("id_arb_aj");
        $datosArbitroEditado=array(
            "nombre_arb_aj"=>$this->input->post("nombre_arb_aj"),
            "apellido_arb_aj"=>$this->input->post("apellido_arb_aj"),
            "email_aj"=>$this->input->post("email"),
            "password_arb_aj"=>$this->input->post("password_arb_aj")
          );

        if ($this->arbitro->actualizar($id_arb_aj,$datosArbitroEditado)) {
          // echo "INSERCION EXITOSA";
          redirect("arbitros/index");
        }
        else {
          echo "ERROR AL INSERTAR";
        }
      }


      public function guardarArbitro(){
        $datosNuevoArbitro=array(
          "nombre_arb_aj"=>$this->input->post("nombre_arb_aj"),
          "apellido_arb_aj"=>$this->input->post("apellido_arb_aj"),
          "email_aj"=>$this->input->post("email_aj"),
          "password_arb_aj"=>$this->input->post("password_arb_aj")
        );
            if ($this->arbitro->insertar($datosNuevoArbitro)) {
        $this->session->set_flashdata("confirmacion","Arbitro insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevvamente");
      }
        redirect("arbitros/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_arb_aj){

        if($this->arbitro->eliminar($id_arb_aj)){
          $this->session->set_flashdata("confirmacion","Calendario eliminado exitosamente.");
                  redirect("arbitros/index");
        }else{
            $this->session->set_flashdata("error al eliminar exitosamente.");
        }
        redirect("arbitros/index");
    }

} //Cierre de la clase
?>
