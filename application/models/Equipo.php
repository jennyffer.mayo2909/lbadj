<?php
class Equipo extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      public function consultarTodos(){
        $listadoEquipos=$this->db->get('equipo');
        if ($listadoEquipos->num_rows()>0) {
          // Cuando hay equipos
          return $listadoEquipos;
        }else {
          // Cuando no hay equipos
          return false;
        }
      }

      public function consultarPorId($id_equi_aj){
      $this->db->where("id_equi_aj",$id_equi_aj);
        $equipo=$this->db->get("equipo");
        if($equipo->num_rows()>0){
          return $equipo->row();//cuando SI hay equipos
        }else{
          return false;//cuando NO hay equipos
        }
      }

      public function actualizar($id_equi_aj,$datos){
        $this->db->where("id_equi_aj",$id_equi_aj);
      return $this->db->update("equipo",$datos);
    }

    public function insertar($datos){
    return $this->db->insert("equipo",$datos);
  }
    // FUNCIÒN PARA ELIMINAR
    public function eliminar($id_equi_aj){
      $this->db->where("id_equi_aj",$id_equi_aj);
      return $this->db->delete("equipo");
    }

  }
?>
