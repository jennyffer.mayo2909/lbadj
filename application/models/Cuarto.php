<?php
 class  Cuarto extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("cuarto",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $cuartos=$this->db->get("cuarto");
  if ($cuartos ->num_rows()>0) {
    return $cuartos;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_cu_ja){
        $this->db->where("id_cu_ja",$id_cu_ja);
        return $this->db->delete("cuarto");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_cu_ja",$id);
      $cuarto=$this->db->get("cuarto");
      if($cuarto->num_rows()>0){
        return $cuarto->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_cu_ja",$id);
     $this->db->update ("cuarto",$datos);
   }
}
