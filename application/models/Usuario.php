<?php
    class Usuario extends CI_Model{
      public function __construct(){
        parent::__construct();
      }

      //consulta para el Login de Usuario
      public function buscarUsuarioPorEmailPassword($email_usu_aj,
                      $password_usu_aj){
            $this->db->where("email_usu_aj",$email_usu_aj);
            $this->db->where("password_usu_aj",$password_usu_aj);
            $usuarioEncontrado=$this->db->get("usuario");
            if($usuarioEncontrado->num_rows()>0){
              return $usuarioEncontrado->row();
            }else{//cuando las credenciales estan incorrectas
              return false;
            }
      }
      public function insertarUsuario($data){
        return $this->db->insert("usuario",$data);

      }
      public function obtenerTodos(){
        $listado=$this->db->get("usuario");
        if($listado->num_rows()>0){
          return $listado;
        }else{//cuando las credenciales estan incorrectas
          return false;
        }

      }
      public function eliminar($ide_usu_aj){
      $this->db->where("ide_usu_aj",$ide_usu_aj);
    return  $this->db->delete("usuario");
    }

    }//cierre de la clase Usuario
