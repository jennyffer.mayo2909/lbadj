<?php
class Calendario extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      public function consultarTodos(){
        // $this->db->join('equipo','equipo.fk_id_equipo1=calendarios.id_cal');
        // $this->db->join('arbitro','arbitro.id_arb=calendarios.fk_id_arb');
        $listadoCalendarios=$this->db->get('calendarios');
        if ($listadoCalendarios->num_rows()>0) {
          // Cuando hay calendarios
          return $listadoCalendarios;
        }else {
          // Cuando no hay calendarios
          return false;
        }
      }

      public function consultarPorId($id_cal_aj){
      $this->db->where("id_cal_aj",$id_cal_aj);
        $calendario=$this->db->get("calendarios");
        if($calendario->num_rows()>0){
          return $calendario->row();//cuando SI hay calendarios
        }else{
          return false;//cuando NO hay calendarios
        }
      }

      public function actualizar($id_cal_aj,$datos){
        $this->db->where("id_cal_aj",$id_cal_aj);
      return $this->db->update("calendarios",$datos);
    }

    public function insertar($datos){
    return $this->db->insert("calendarios",$datos);
  }
    // FUNCIÒN PARA ELIMINAR
    public function eliminar($id_cal_aj){
      $this->db->where("id_cal_aj",$id_cal_aj);
      return $this->db->delete("calendarios");
    }

  }
?>
