<?php
class Arbitro extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      public function consultarTodos(){
        $listadoArbitros=$this->db->get('arbitro');
        if ($listadoArbitros->num_rows()>0) {
          // Cuando hay equipos
          return $listadoArbitros;
        }else {
          // Cuando no hay equipos
          return false;
        }
      }

      public function consultarPorId($id_arb_aj){
      $this->db->where("id_arb_aj",$id_arb_aj);
        $arbitro=$this->db->get("arbitro");
        if($arbitro->num_rows()>0){
          return $arbitro->row();
        }else{
          return false;
        }
      }

      public function actualizar($id_arb_aj,$datos){
        $this->db->where("id_arb_aj",$id_arb_aj);
      return $this->db->update("arbitro",$datos);
    }

    public function insertar($datos){
    return $this->db->insert("arbitro",$datos);
  }
    // FUNCIÒN PARA ELIMINAR
    public function eliminar($id_arb_aj){
      $this->db->where("id_arb_aj",$id_arb_aj);
      return $this->db->delete("arbitro");
    }

  }
?>
