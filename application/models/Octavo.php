<?php
 class  Octavo extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("octavo",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $octavos=$this->db->get("octavo");
  if ($octavos ->num_rows()>0) {
    return $octavos;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_oc_ja){
        $this->db->where("id_oc_ja",$id_oc_ja);
        return $this->db->delete("octavo");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_oc_ja",$id);
      $octavo=$this->db->get("octavo");
      if($octavo->num_rows()>0){
        return $octavo->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_oc_ja",$id);
     $this->db->update ("octavo",$datos);
   }
}
