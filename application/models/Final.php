<?php
 class  Final extends CI_Model
 {
  public function __construct()
  {
   parent::__construct();
}
public function insertar($datos){
  return $this->db->insert("final",$datos);
}
// consulta todos los estudiantes d ela base de datos
public function obtenerTodos(){
  $finales=$this->db->get("final");
  if ($finales ->num_rows()>0) {
    return $finales;
  }else{
    return false; //cuando no existen los datos
  }

}
public function eliminarPorId($id_fin_ja){
        $this->db->where("id_fin_ja",$id_fin_ja);
        return $this->db->delete("final");
      }
// consultando al estudiante por id
public function obtenerporId($id){
      $this->db->where("id_fin_ja",$id);
      $final=$this->db->get("final");
      if($final->num_rows()>0){
        return $final->row(); // solo biene uno
      }else{
        return false;
      }
   }
   // proceso de actualizacion del estudiante
   public function actualizar($id,$datos){
     $this->db->where ("id_fin_ja",$id);
     $this->db->update ("final",$datos);
   }
}
